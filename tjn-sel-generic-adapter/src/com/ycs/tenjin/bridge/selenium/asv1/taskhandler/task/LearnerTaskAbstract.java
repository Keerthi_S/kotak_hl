/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  GenericLearnerImpl.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* Sep 1, 2016           Sriram Sridharan          Newly Added For 
 * 08-09-2017			Sameer Gupta			Rewritten for New Adapter Spec
 * 26-09-2018			Preeti					TENJINCG-742
*/

package com.ycs.tenjin.bridge.selenium.asv1.taskhandler.task;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.ycs.tenjin.bridge.enums.FieldType;
import com.ycs.tenjin.bridge.enums.PageType;
import com.ycs.tenjin.bridge.exceptions.AssistedLearningException;
import com.ycs.tenjin.bridge.exceptions.AutException;
import com.ycs.tenjin.bridge.exceptions.LearnerException;
import com.ycs.tenjin.bridge.pojo.aut.Location;
import com.ycs.tenjin.bridge.pojo.aut.Metadata;
import com.ycs.tenjin.bridge.pojo.aut.Module;
import com.ycs.tenjin.bridge.pojo.aut.TestObject;
import com.ycs.tenjin.bridge.selenium.asv1.SeleniumApplicationImpl;
import com.ycs.tenjin.bridge.selenium.asv1.generic.DriverContext;
import com.ycs.tenjin.bridge.selenium.asv1.generic.ElementIdentificationException;
import com.ycs.tenjin.bridge.utils.AssistedLearningUtils;
import com.ycs.tenjin.util.Utilities;

public abstract class LearnerTaskAbstract implements LearnerTask {

	private static final Logger logger = LoggerFactory.getLogger(LearnerTaskAbstract.class);
	protected SeleniumApplicationImpl oApplication;
	protected Module function;
	protected ArrayList<TreeMap<String, TestObject>> testObjectMap;
	protected Multimap<String, Location> pageAreas;
	protected DriverContext driverContext;
	protected int pageSequence = 0;
	protected int fieldSequence = 0;
	protected List<TestObject> testObjects = new ArrayList<TestObject>();
	protected Multimap<String, TestObject> learningData = null;
	protected String learningType;
	
	@Override
	public Metadata learn() throws LearnerException {
		
		
		Metadata metadata = new Metadata();
		
		logger.info("Beginning learning of function {}", this.function.getCode());
		
		this.driverContext = this.oApplication.getDriverContext();
		
		logger.info("Loading assisted learning data");
		//Get the Learning data
		try{
			/*Modified by Preeti for TENJINCG-742 starts*/
			/*this.learningData = AssistedLearningUtils.getAssistedLearningData(this.function.getCode(), this.oApplication.getAppName(), null);*/
			this.learningData = AssistedLearningUtils.getAssistedLearningData(this.function.getCode(), this.oApplication.getAppId());
			/*Modified by Preeti for TENJINCG-742 ends*/
		} catch (AssistedLearningException e) {
			logger.error("ERROR loading assisted learning data for function {}", this.function.getCode(), e);
		}
		
		//At this point, we will be in the landing page of a given function. Update the location as such.
		
		Location location = new Location();
		location.setLabel(this.driverContext.getCurrentLocation());
		location.setLandingPage(true);
		location.setLocationType(PageType.MAIN.toString());
		location.setParent("N-A");
		location.setSequence(this.pageSequence++);
		location.setWayIn("Navigate to Function");
		location.setWayOut("Exit");
		location.setScreenTitle(this.driverContext.getCurrentLocation());
		try{
			logger.info("Setting  main location to " + location.getLocationName());
			this.addToMap(location);
			this.driverContext.setUniqueLocationName(location.getLocationName());
		}catch(Exception e){
			throw new LearnerException(e.getMessage(),e);
		}
		/* Commented by sameer for New Adapter Spec */
//		long startMillis = System.currentTimeMillis();
		this.learningType="N";
		if(this.learningType.equalsIgnoreCase("S")){
			selectivenavigateAndLearn();
		}
		else{
			navigateAndLearn();
		}
		metadata.setPageAreas(this.pageAreas);
		metadata.setTestObjectMap(this.testObjectMap);
		return metadata;
	}
	
	public LearnerTaskAbstract(SeleniumApplicationImpl oApplication, Module function){
		this.oApplication = oApplication;
		this.function = function;
		this.pageAreas = ArrayListMultimap.create();
		this.testObjectMap = new ArrayList<TreeMap<String,TestObject>>();
		this.learningType="S";
	}
	
	protected void learnScreen(TreeMap<String, TestObject> pMap){
		
		logger.info("Learning screen {}", this.driverContext.getUniqueLocationName());
		if (pMap == null) {
			pMap = new TreeMap<String, TestObject>();
		}
		this.testObjects = new ArrayList<TestObject>();
		
		try{
			this.getAllVisibleElements(this.driverContext.getWrapper());
		}catch(Exception e){
			logger.error("Could not get all visible elements on {}", this.driverContext.getCurrentLocation());
		}
		
		for(TestObject t:this.testObjects){
			pMap.put(t.getUniqueId(), t);
		}
		
		this.testObjects.clear();
		
		//this.testObjectMap.add(pMap);
		
		this.processMultiRecordBlocks();
	}
	
	protected void learnScreen(WebElement section, TreeMap<String, TestObject> pMap){
		
		logger.info("Learning screen {}", this.driverContext.getUniqueLocationName());
		if (pMap == null) {
			pMap = new TreeMap<String, TestObject>();
		}
		this.testObjects = new ArrayList<TestObject>();
		
		try{
			this.getAllVisibleElements(section);
		}catch(Exception e){
			logger.error("Could not get all visible elements on {}", this.driverContext.getCurrentLocation());
		}
		
		for(TestObject t:this.testObjects){
			pMap.put(t.getUniqueId(), t);
		}
		
		this.testObjects.clear();
		
		//this.testObjectMap.add(pMap);
		
		//this.processMultiRecordBlocks();
	}
	
	protected void learnScreen(){
		
		logger.info("Learning screen {}", this.driverContext.getUniqueLocationName());
		TreeMap<String, TestObject> pMap = new TreeMap<String, TestObject>();
		this.testObjects = new ArrayList<TestObject>();
		
		try{
			this.getAllVisibleElements(this.driverContext.getWrapper());
		}catch(Exception e){
			logger.error("Could not get all visible elements on {}", this.driverContext.getCurrentLocation());
		}
		
		for(TestObject t:this.testObjects){
			pMap.put(t.getUniqueId(), t);
		}
		
		this.testObjects.clear();
		
		this.testObjectMap.add(pMap);
		
		this.processMultiRecordBlocks();
	}
	
	protected void learnScreen(WebElement section){
		
		logger.info("Learning screen {}", this.driverContext.getUniqueLocationName());
		TreeMap<String, TestObject> pMap = new TreeMap<String, TestObject>();
		this.testObjects = new ArrayList<TestObject>();
		
		try{
			this.getAllVisibleElements(section);
		}catch(Exception e){
			logger.error("Could not get all visible elements on {}", this.driverContext.getCurrentLocation());
		}
		
		for(TestObject t:this.testObjects){
			pMap.put(t.getUniqueId(), t);
		}
		
		this.testObjects.clear();
		
		this.testObjectMap.add(pMap);
		
		//this.processMultiRecordBlocks();
	}
	
	protected void addToMap(Location location) throws LearnerException{
		try{
			String presentLocationName;
			String locationName = location.getLabel();
			logger.debug("Scanning map for existing pages with name " + locationName);
			Collection<Location> locs = this.pageAreas.get(locationName);
			if(locs != null && locs.size() > 0){
				logger.debug("There is already " + locs.size() + " page(s) with name " + locationName);
				int nc = locs.size() + 1;
				String newLocName = "00" + Integer.toString(nc);
				newLocName = newLocName.substring(newLocName.length() -2);
				newLocName = locationName + " " + newLocName;
				logger.debug("New Location Name will be " + newLocName);
				
				/*****************************************************************************
				 * Added by Sharath for Valid Name for Location Starts
				 */
				
				logger.info("Location Name Before Changing is : " + newLocName);
				presentLocationName = checkForValidName(newLocName);
				logger.info("Location Name After Changing is : " + presentLocationName);
				location.setLocationName(presentLocationName);
				/*****************************************************************************
				 * Added by Sharath for Valid Name for Location Ends
				 */
			}else{
				logger.debug("There are no existing pages with name " + locationName);
				/*****************************************************************************
				 * Added by Sharath for Valid Name for Location Starts
				 */
				logger.info("Location Name Before Changing is : " + locationName);
				presentLocationName = checkForValidName(locationName);
				logger.info("Location Name After Changing is : " + presentLocationName);
				location.setLocationName(presentLocationName);
				/*****************************************************************************
				 * Added by Sharath for Valid Name for Location Ends
				 */
			}

			this.pageAreas.put(locationName, location);
			logger.info("New Location --> {}", location.toString());
		}catch(Exception e){
			throw new LearnerException("Could not resolve location name for " + location.getLocationName() ,e);
		}
	}
	
	/*****************************************************************************
	 * Added by Sharath for Valid Name for Location Starts
	 */
	
	public static String checkForValidName(String name){
		String changedName=name;
		/*if(name==null || name.equals("")){
			logger.info("page Area name should not be null ");
			throw new LearningException("page Area name should not be null ");
		}else{
			String[] inValid={"/","\\","*","[","]",":","?"};
			for(int i=0;i<inValid.length;i++){
				if(name.contains(inValid[i]))
				{
					changedName=name.replace('/', '-');
					logger.info("page Area name has been changed from {} to {}",name,changedName);
				}
			}
		}*/
		return changedName;
	}
	
	/*****************************************************************************
	 * Added by Sharath for Valid Name for Location Ends
	 */
	
	
	protected void getAllVisibleElements(WebElement wrapper) {
		String xpathQuery = this.oApplication.getCompositeXpath();
		
		try{

			List<WebElement> elements = this.oApplication.locateElements(wrapper, xpathQuery,"xpath");
			logger.info("{} elements found to scan on {}", elements.size(), this.driverContext.getCurrentLocation());
			if(elements != null){
				
				int counter=0;
				int tabOrder = 0;
				for(WebElement element:elements){
					counter++;
					logger.debug("Processing element {}", counter);
					if(!element.isDisplayed()){
						/*if(element.getAttribute("type") != null && element.getAttribute("type").equalsIgnoreCase("hidden")){
							
						}else{*/
						logger.warn("Element {} is hidden. Will be skipped", counter);
						continue;
						/*}*/
					}
					
					
					Map<String, String> attributesMap = this.getHTMLAttributesOfElement(element);
					logger.debug("All element attributes --> {}", attributesMap.get("all"));
					
					String elementType = this.getElementType(attributesMap);
					logger.debug("Element Type --> {}", elementType);
					
					
					Map<String, String> uidMap = this.getUniqueIdOfElement(attributesMap, element);
					logger.debug("UID Map --> {}", uidMap);
					
					if(Utilities.trim(uidMap.get("uid")).equalsIgnoreCase("")){
						logger.warn("Could not get Unique ID for element {}. This element will be skipped", counter);
						continue;
					}
					
					String label = this.getLabelOfElement(attributesMap, element);
					logger.debug("Label --> {}", label);
					
					if(Utilities.trim(label).equalsIgnoreCase("")){
						logger.warn("Label empty. So using UID as label");
						label = uidMap.get("uid");
						logger.debug("New Label --> {}", label);
					}
					
					boolean mandatory = this.isMandatory(attributesMap, element);
					logger.debug("Mandatory --> {}", mandatory);
					
					String defaultOptions = this.getDefaultOptionsForElement(attributesMap, element);
					logger.debug("Default Options --> {}", defaultOptions);
					
					
					boolean hasLov = this.elementHasLOV(attributesMap, element);
					logger.debug("Has LOV --> {}", hasLov);
					
					/***************************************************************************************************************
					 * Added by Sharath for Auto Lov Starts
					 */
					
					boolean hasAutoLov = this.elementHasAutoLOV(attributesMap, element);
					logger.debug("Has AutoLOV --> {}", hasAutoLov);
					
					/***************************************************************************************************************
					 * Added by Sharath for Auto Lov Ends
					 */
					
					String fieldGroup = this.getFieldGroup(attributesMap, element);
					logger.debug("Field Group --> {}", fieldGroup);
					
					TestObject t = new TestObject();
					t.setLabel(label);
					t.setUniqueId(uidMap.get("uid"));
					t.setIdentifiedBy(uidMap.get("uidType"));
					t.setObjectClass(elementType);
					t.setDefaultOptions(defaultOptions);
					if(mandatory){
						t.setMandatory("YES");
					}else{
						t.setMandatory("NO");
					}
					
					if(hasLov){
						t.setLovAvailable("Y");
					}else{
						t.setLovAvailable("N");
					}
					
					/***************************************************************************************************************
					 * Added by Sharath for Auto Lov Starts
					 */
					
					if(hasAutoLov){
						t.setAutoLovAvailable("Y");
					}else{
						t.setAutoLovAvailable("N");
					}
					
					/***************************************************************************************************************
					 * Added by Sharath for Auto Lov Ends
					 */
					
					
					t.setViewmode("Y");
					t.setTabOrder(tabOrder++);
					t.setSequence(this.fieldSequence++);
					if(!Utilities.trim(this.driverContext.getUniqueLocationName()).equalsIgnoreCase("")){
						t.setLocation(this.driverContext.getUniqueLocationName());
					}else{
						t.setLocation(this.driverContext.getCurrentLocation());
					}
					
					if(Utilities.trim(fieldGroup).length() > 0){
						t.setIsMultiRecord("Y");
						t.setGroup(fieldGroup);
					}else{
						t.setIsMultiRecord("N");
						t.setGroup("");
					}
					this.testObjects.add(t);
					
					logger.info("Element found --> {}, {}, {} identified by {}", elementType, label, uidMap.get("uid"), uidMap.get("idType"));
				}
			}
		
		}catch(Exception e){
			logger.error("ERROR in getAllVisibleElements() - Unable to scan elements in page {}", this.driverContext.getCurrentLocation(), e);
			throw new LearnerException("Could not scan elements on page " + this.driverContext.getCurrentLocation());
		}
	}
	
protected Map<String, String> getHTMLAttributesOfElement(WebElement element){
		
		try {
			@SuppressWarnings("unchecked")
			ArrayList<String> parentAttributes = (ArrayList<String>) ((JavascriptExecutor)this.driverContext.getDriver()).executeScript(
					"var s = []; var attrs = arguments[0].attributes; for (var l = 0; l < attrs.length; ++l) { var a = attrs[l]; s.push(a.name + ':' + a.value); } ; return s;", element);
			
			Map<String, String> attributesMap = new HashMap<String, String>();
			String allAttrs = "";
			String aValue =null;
			for(String s:parentAttributes){
				String[] split = s.split(":");
				if(split.length >=2){
					if(split.length == 2){
						attributesMap.put(split[0], split[1]);
					}else{
						aValue = "";
						for(int i=1;i<split.length;i++){
							if(i < split.length-1){
								aValue = aValue + split[i] + ":";
							}else{
								aValue = aValue + split[i];
							}
						}
						
						attributesMap.put(split[0], aValue);
					}
				}else{
					attributesMap.put(split[0], null);
				}

				allAttrs = allAttrs + s + ",";
			}
			
			attributesMap.put("all",allAttrs);
			attributesMap.put("tag", element.getTagName());
			
			return attributesMap;
		} catch (Exception e) {
			
			logger.error("ERROR while getting HTML attributes of element", e);
			return null;
		}
	}
	
	protected String getElementType(Map<String, String> attributesMap){
		String elementDesc = null;
		String type = "";
		type = attributesMap.get("type");
		if(type == null || type.equalsIgnoreCase("")){
			type = attributesMap.get("tag");
		}

		

		if(type != null && type.equalsIgnoreCase("text")){
			elementDesc = FieldType.TEXTBOX.toString();
		}else if(type != null && type.equalsIgnoreCase("password")){
			elementDesc = FieldType.PASSWORD.toString();
		}else if(type != null && type.equalsIgnoreCase("checkbox")){
			elementDesc = FieldType.CHECKBOX.toString();
		}else if(type != null && type.toLowerCase().contains("radio")){
			elementDesc = FieldType.RADIO.toString();
		}else if(type != null && type.equalsIgnoreCase("button")){
			elementDesc = FieldType.BUTTON.toString();
		}else if(type != null && type.equalsIgnoreCase("submit")){
			elementDesc = FieldType.BUTTON.toString();
		}else if(type != null && type.equalsIgnoreCase("file")){
			elementDesc = FieldType.FILE.toString();
		}else if(type != null && type.equalsIgnoreCase("select")){
			elementDesc = FieldType.LIST.toString();
		}else if(type != null && type.equalsIgnoreCase("a")){
			elementDesc = FieldType.LINK.toString();
		}else if(type != null && type.equalsIgnoreCase("table")){
			elementDesc = FieldType.TABLE.toString();
		}else if(type != null && type.equalsIgnoreCase("button")){
			elementDesc = FieldType.BUTTON.toString();
		}else if(type != null && type.equalsIgnoreCase("textarea")){
			elementDesc = FieldType.TEXTAREA.toString();
		}else if(type != null && type.equalsIgnoreCase("me")){
			elementDesc = FieldType.TABLE.toString();
		}else{
			logger.error("Element type not found");
		}
		
		attributesMap.put("elem_type", elementDesc);

		return elementDesc;

	
	}
	
	protected Map<String ,String> getUniqueIdOfElement(Map<String, String> attributesMap, WebElement element) throws LearnerException{
		Map<String, String> uidMap = new HashMap<String, String>();
		
		String uid = "";
		String identifiedBy = "";
		if(!attributesMap.get("elem_type").equalsIgnoreCase(FieldType.RADIO.toString())){
			uid = Utilities.trim(attributesMap.get("id"));
			if(uid.equalsIgnoreCase("")){
				uid = Utilities.trim(attributesMap.get("name"));
				if(!uid.equalsIgnoreCase("")){
					identifiedBy = "name";
				}else{
					logger.error("Could not identify UID of element. ID or Name not found");
					logger.error("All attrs --> {}", attributesMap.get("all"));
					return null;
				}
			}else{
				identifiedBy = "id";
			}
		}else{
			uid = Utilities.trim(attributesMap.get("name"));
			identifiedBy = "name";
			
			if(uid.equalsIgnoreCase("")){
				logger.error("Could not identify UID for RADIO button. NAME attribute not defined");
				//throw new LearningException("Could not identify UID for RADIO button. NAME attribute not defined");
			}
		}
		
		uidMap.put("uid", uid);
		uidMap.put("uidType", identifiedBy);
		
		return uidMap;
	}
	
	protected Map<String, Object> performAssistedLearningInput() throws LearnerException{
		boolean assistedLearningInputPerformed = false;
		String lastClickedItem = "";
		String currentLocation = Utilities.trim(this.driverContext.getUniqueLocationName());
		logger.debug("Entering Assisted Learning Input for screen --> " + currentLocation);
		
		if(this.learningData != null){
			Collection<TestObject> fields = this.learningData.get(currentLocation);
			logger.debug("Found {} fields to input on this screen", fields.size());
			
			for(TestObject field:fields){
				
				//Get the UID of the field
				String label = field.getLabel();
				logger.debug("Field label --> {}, value --> {}", field.getLabel(), field.getData());
				
				String finalLabel = "";
				int instance =0;
				String inst = "";
				if(field.getLabel().contains("(") && field.getLabel().contains(")")){
					finalLabel = label.substring(0, label.indexOf("("));
					finalLabel = Utilities.trim(finalLabel);
					inst = label.substring(label.indexOf("(")+1, label.indexOf(")"));

					if(Utilities.isNumeric(inst)){
						instance = Integer.parseInt(inst);
						instance = instance-1;
					}else{
						instance = 0;
						finalLabel = field.getLabel();
					}
				}
				logger.debug("Final label --> {}", finalLabel);
				
				if(field.getIdentifiedBy() != null && field.getIdentifiedBy().toLowerCase().startsWith("toolbar")){
					logger.debug("Selecting toolbar item {}", field.getLabel());
					try {
						this.driverContext = this.oApplication.clickToolbarItem(field.getLabel());
					} catch (AutException e) {
						
						throw new LearnerException(e.getMessage());
					}
					assistedLearningInputPerformed = true;
					lastClickedItem = FieldType.TOOLBARITEM.toString() + ";" + field.getLabel();
				}
				
				/****************************************************************************************************
				 *  Added by Sharath for Handling Frames and links Starts
				 */
				
				else if(field.getIdentifiedBy() != null && field.getIdentifiedBy().toLowerCase().startsWith("link")){
					logger.debug("Selecting navigation item {}", field.getLabel());

					try {
						this.driverContext = this.oApplication.navigateToLink(field.getLabel());
					} catch (AutException e) {
						
						throw new LearnerException(e.getMessage());
					}
					assistedLearningInputPerformed = true;
					lastClickedItem = FieldType.LINK.toString() + ";" + field.getLabel();
				}
				
				
				/*else if(field.getData().startsWith("Enter")){
					logger.debug("performing Key Operation {}", field.getLabel());
					if(field.getLabel().equalsIgnoreCase("Enter")){
						
					}
					assistedLearningInputPerformed = true;
					lastClickedItem = "Navigationlink;" + field.getLabel();
				}*/
				/****************************************************************************************************
				 *  Added by Sharath for Handling Frames and links Ends
				 */
				
				else{
					ArrayList<TestObject> matchingFields = new ArrayList<TestObject>();
					TestObject targetField = null;
					for(TreeMap<String,TestObject> map:this.testObjectMap){
						for(String key:map.keySet()){
							TestObject t = map.get(key);
							if(field.getLabel().contains("(") && field.getLabel().contains(")") && Utilities.isNumeric(inst)){
								if(Utilities.trim(t.getLocation()).equalsIgnoreCase(currentLocation) && Utilities.trim(t.getLabel()).equalsIgnoreCase(finalLabel)){
									matchingFields.add(t);
								}
							}else if(field.getIdentifiedBy() != null && field.getIdentifiedBy().equalsIgnoreCase("id")){
								if(Utilities.trim(t.getLocation()).equalsIgnoreCase(currentLocation) && Utilities.trim(t.getUniqueId()).equalsIgnoreCase(field.getLabel())){
									targetField = t;
									break;
								}
							}else{
								if(Utilities.trim(t.getLocation()).equalsIgnoreCase(currentLocation) && Utilities.trim(t.getLabel()).equalsIgnoreCase(field.getLabel())){
									targetField = t;
									break;
								}
							}
						}
					}

					if(field.getLabel().contains("(") && field.getLabel().contains(")") && Utilities.isNumeric(inst)){
						if(matchingFields != null && matchingFields.size() >= instance){
							targetField = matchingFields.get(instance);
						}
					}

					if(targetField == null){

						if(field.getLabel().equalsIgnoreCase("@@LEARN")){
							TreeMap<String, TestObject> pMap = null;
							logger.info("Learning screen {} once again", this.driverContext.getCurrentLocation());
							try{

								int mapIndex = 0;
								for(TreeMap<String, TestObject> map:this.testObjectMap){
									for(String key:map.keySet()){
										TestObject t = map.get(key);
										if(Utilities.trim(t.getLocation()).equalsIgnoreCase(currentLocation)){
											pMap = map;
											break;
										}
									}
									if(pMap != null){
										break;
									}
									mapIndex++;
								}

								if(pMap == null){
									logger.error("ERROR could not find test object map for location {}", currentLocation);
									pMap = new TreeMap<String, TestObject>();
								}else{
									try {
										this.testObjectMap.remove(mapIndex);
									} catch (Exception e) {
										
										//logger.error("Error ", e);

										logger.error(e.getMessage(),e);
									}
								}

								logger.debug("GetVisibleElements --> {}", this.driverContext.getCurrentLocation());
								this.getAllVisibleElements(this.driverContext.getWrapper());
								logger.info("Found {} elements on {}", this.testObjects.size(), this.driverContext.getCurrentLocation());
								int pageTabOrder=0;
								for(TestObject t:this.testObjects){
									pageTabOrder++;
									t.setTabOrder(pageTabOrder);
									pMap.put(t.getUniqueId(), t);
								}
								this.testObjects.clear();
							}catch(LearnerException e){
								logger.error("Could not get visible elements on page {}", this.driverContext.getCurrentLocation());
							}catch(Exception e){
								logger.error("Could not get visible elements on page {}", this.driverContext.getCurrentLocation());
								logger.error(e.getMessage());
							}

							this.testObjectMap.add(pMap);
							logger.info("Added processed test objects from {} to map", this.driverContext.getCurrentLocation());
							continue;
						}else{
							logger.error("Could not find information about field {} for assisted learning input", field.getLabel());
							throw new LearnerException("Could not find information about field " + field.getLabel() + " for assisted learning input");
						}

					}else{
						logger.debug("Found target field");
						List<WebElement> elements = null;
						WebElement targetele = null;
						try {
							elements = oApplication.locateElements(targetField.getUniqueId(), targetField.getIdentifiedBy());
							for (WebElement element : elements) {
								if(element.isDisplayed()){
									targetele = element;
								}
							}
						} catch (ElementIdentificationException e) {
							
							logger.debug("ERROR finding element in the screen {}",currentLocation);
						}
						
						if(FieldType.BUTTON.toString().equalsIgnoreCase(targetField.getObjectClass())){
							logger.debug("Selecting navigation item {}", field.getLabel());
							try {
								this.driverContext = this.oApplication.navigateToButton(targetele);
							} catch (AutException e) {
								
								throw new LearnerException(e.getMessage());
							}
							assistedLearningInputPerformed = true;
							lastClickedItem = FieldType.BUTTON.toString() + ";" + field.getLabel();
							continue;
						}else{
							try {
								this.oApplication.performInput(targetele, targetField, field.getData());
							} catch (AutException e) {
								
								logger.error("Error ", e);
							}
							assistedLearningInputPerformed = true;
							continue;
						}
					}


				}
				
				
				
			}
		}
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("inputPerformed", assistedLearningInputPerformed);
		map.put("lastClickedItem", lastClickedItem);
		
		return map;
	}
	
	protected abstract void navigateAndLearn() throws LearnerException;
	
	protected abstract String getLabelOfElement(Map<String, String> attributesMap, WebElement element) throws LearnerException;
	
	protected abstract boolean isMandatory(Map<String, String> attributesMap, WebElement element) throws LearnerException;
	
	protected abstract String getDefaultOptionsForElement(Map<String, String> attributesMap, WebElement element) throws LearnerException;
	
	protected abstract String getViewModeOfElement(Map<String, String> attributesMap, WebElement element) throws LearnerException;
	
	protected abstract boolean elementHasLOV(Map<String, String> attributesMap, WebElement element) throws LearnerException;
	
	protected abstract void processMultiRecordBlocks() throws LearnerException;
	
	protected abstract String getFieldGroup(Map<String, String> attributesMap, WebElement element) throws LearnerException;
	
	protected abstract boolean elementHasAutoLOV(Map<String, String> attributesMap, WebElement element);

	protected void selectivenavigateAndLearn() throws LearnerException{}
	
}
