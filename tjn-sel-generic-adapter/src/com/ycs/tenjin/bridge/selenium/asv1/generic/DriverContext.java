/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  DriverContext.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* Sep 1, 2016           Sriram Sridharan          Newly Added For 
*/

package com.ycs.tenjin.bridge.selenium.asv1.generic;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class DriverContext {
	
	private String currentLocation;
	private String uniqueLocationName;
	private WebElement wrapper;
	private List<String> tabs;
	private List<WebElement> tabElements;
	private List<WebElement> links;
	private List<WebElement> buttons;
	private List<WebElement> wrappers;
	private List<String> toolbarItems;
	private List<WebElement> toolbarItemElements;
	private String initWindowHandle;
	private WebDriver driver;
	private String uniqueFrameId;
	private boolean contextSame;
	private WebElement frameExitButton;
	private AutAlert recentAlert;
	private String activeTab;
	
	/*******************************************************************************************
	 * Added by Sharath for Frame handling starts
	 */

	private List<String> frameItems;
	private List<WebElement> frameItemElements;
	
	public List<String> getFrameItems() {
		return frameItems;
	}
	public void setFrameItems(List<String> frameItems) {
		this.frameItems = frameItems;
	}
	public List<WebElement> getFrameItemElements() {
		return frameItemElements;
	}
	public void setFrameItemElements(List<WebElement> frameItemElements) {
		this.frameItemElements = frameItemElements;
	}
	
	/******************************************************************************************
	 * Added by Sharath for Frame handling Ends
	 */

	/*******************************************************************************************
	 * Added by Sharath for Frame handling starts
	 */

	private List<String> navigationItems;
	@SuppressWarnings("unused")
	private List<WebElement> navigationItemElements;
	
	public List<String> getNavigationItems() {
		return navigationItems;
	}
	public void setNavigationItems(List<String> navigationItems) {
		this.navigationItems = navigationItems;
	}
	public List<WebElement> getNavigationElements() {
		return frameItemElements;
	}
	public void setNavigationElements(List<WebElement> navigationItemElements) {
		this.navigationItemElements = navigationItemElements;
	}
	
	/******************************************************************************************
	 * Added by Sharath for Frame handling Ends
	 */
	
	/******************************************************************************************
	 * Added by Sharath for recover Starts
	 */

	private boolean defaultContent;
	private AutMessage autMessage;
	
	public boolean isDefaultContent() {
		return defaultContent;
	}
	public void setDefaultContent(boolean defaultContent) {
		this.defaultContent = defaultContent;
	}
	
	/******************************************************************************************
	 * Added by Sharath for recover Ends
	 */

	
	public AutMessage getAutMessage() {
		return this.autMessage;
	}
	public void setAutMessage(AutMessage autError) {
		this.autMessage = autError;
	}
	public WebElement getWrapper() {
		return wrapper;
	}
	public void setWrapper(WebElement wrapper) {
		this.wrapper = wrapper;
	}
	public String getActiveTab() {
		return activeTab;
	}
	public void setActiveTab(String activeTab) {
		this.activeTab = activeTab;
	}
	public List<WebElement> getTabElements() {
		return tabElements;
	}
	public void setTabElements(List<WebElement> tabElements) {
		this.tabElements = tabElements;
	}
	public List<WebElement> getToolbarItemElements() {
		return toolbarItemElements;
	}
	public void setToolbarItemElements(List<WebElement> toolbarItemElements) {
		this.toolbarItemElements = toolbarItemElements;
	}
	public List<String> getToolbarItems() {
		return toolbarItems;
	}
	public void setToolbarItems(List<String> toolbarItems) {
		this.toolbarItems = toolbarItems;
	}
	public AutAlert getRecentAlert() {
		return recentAlert;
	}
	public void setRecentAlert(AutAlert recentAlert) {
		this.recentAlert = recentAlert;
	}
	public WebElement getFrameExitButton() {
		return frameExitButton;
	}
	public void setFrameExitButton(WebElement frameExitButton) {
		this.frameExitButton = frameExitButton;
	}
	public String getCurrentLocation() {
		return currentLocation;
	}
	public void setCurrentLocation(String currentLocation) {
		if(currentLocation != null){
			currentLocation = currentLocation.replaceAll("[/:*?<>|]", "-");
		}
		this.currentLocation = currentLocation;
	}
	public String getUniqueLocationName() {
		return uniqueLocationName;
	}
	public void setUniqueLocationName(String uniqueLocationName) {
		this.uniqueLocationName = uniqueLocationName;
	}
	public List<String> getTabs() {
		return tabs;
	}
	public void setTabs(List<String> tabs) {
		this.tabs = tabs;
	}
	public List<WebElement> getLinks() {
		return links;
	}
	public void setLinks(List<WebElement> links) {
		this.links = links;
	}
	public List<WebElement> getButtons() {
		return buttons;
	}
	public void setButtons(List<WebElement> buttons) {
		this.buttons = buttons;
	}
	public List<WebElement> getWrappers() {
		return wrappers;
	}
	public void setWrappers(List<WebElement> wrappers) {
		this.wrappers = wrappers;
	}
	public String getInitWindowHandle() {
		return initWindowHandle;
	}
	public void setInitWindowHandle(String initWindowHandle) {
		this.initWindowHandle = initWindowHandle;
	}
	public WebDriver getDriver() {
		return driver;
	}
	public void setDriver(WebDriver driver) {
		this.driver = driver;
	}
	public String getUniqueFrameId() {
		return uniqueFrameId;
	}
	public void setUniqueFrameId(String uniqueFrameId) {
		this.uniqueFrameId = uniqueFrameId;
	}
	public boolean isContextSame() {
		return contextSame;
	}
	public void setContextSame(boolean contextSame) {
		this.contextSame = contextSame;
	}
	
	@Override
	public String toString() {
		String r= "DriverContext [currentLocation=" + currentLocation + ", uniqueLocationName=" + uniqueLocationName;
		if(tabs != null && tabs.size() > 0){
			r = r + ", activeTab=" + activeTab;
			r = r + ", tabs={";
			for(String tab:tabs){
				r = r + tab + ",";
			}
			r = r + "}";
		}
		r = r + "]";
		
		if(toolbarItems != null && toolbarItems.size() > 0){
			r = r + ", toolbarItems={";
			for(String tab:toolbarItems){
				r = r + tab + ",";
			}
			r = r + "}";
		}
		r = r + "]";
		
		/***********************************************************************************************************
		 * Added by Sharath for handling Frames and Links Starts
		 */
		
		if(frameItems != null && frameItems.size() > 0){
			r = r + ", frameItems={";
			for(String frame:frameItems){
				r = r + frame + ",";
			}
			r = r + "}";
		}
		r = r + "]";
		
		/***********************************************************************************************************
		 * Added by Sharath for handling Frames and Links Ends
		 */
		
		return r;
	}
	
	/*******************************
	 * Added by Sriram for TCSBancs
	 */
	private WebElement accordionContent;

	public WebElement getAccordionContent() {
		return accordionContent;
	}
	public void setAccordionContent(WebElement accordionContent) {
		this.accordionContent = accordionContent;
	}
	/*******************************
	 * Added by Sriram for TCSBancs ends
	 */
	
	
}
