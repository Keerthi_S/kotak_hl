/***

Yethi Consulting Private Ltd. CONFIDENTIAL


Name of this file:  Launcher.java


Module: TENJIN - INTELLIGENT ENTERPRISE TESTING ENGINE


Copyright � 2016-17 by Yethi Consulting Private Ltd.


This source file is part of the TENJIN Software Product and System 
and is copyrighted by Yethi Consulting Private Ltd.

All rights reserved.  No part of this work may be reproduced, copied, 
duplicated, adopted, distributed, reverse engineered, stored in a retrieval  
system, transmitted in any form or by any means, electronic, 
mechanical, photographic, graphic, optic recording or otherwise, translated 
in any language or computer language, sold, rented, leased without the prior 
written permission of Yethi Consulting Services Private Ltd.

Notice: All information and source code contained in this file is, and remains 
the property of Yethi Consulting Services Private Ltd., and its suppliers, if any. 
The intellectual and technical concepts contained herein are proprietary to Yethi 
Consulting Services Private Ltd., and its suppliers and may be covered under patents 
and patents in process and are protected by trade secret or copyright laws. Dissemination 
of this information or reproduction of this material is strictly forbidden unless prior 
written permission is obtained from Yethi Consulting Services Private Ltd. 


Yethi Consulting Private Ltd.
# 1308, 4th Floor, Shetty Plaza,JB Nagar Main Road,
HAL 3rd Stage, Bangalore - 560 075,
Karnataka-560075,India
	 
*	 
*/

/******************************************
* CHANGE HISTORY
* ==============
*
* DATE                 CHANGED BY              DESCRIPTION
* Sep 1, 2016           Sriram Sridharan          Newly Added For 
* Dec 19, 2016			Sriram					Changed message thrown to include details of browser type and target machine
* 19-12-2016			Sriram					Fix for Defect#TEN-102
 * 08-09-2017			Sameer Gupta			Rewritten for New Adapter Spec
 * 26-02-2021			Ashiki					TENJINCG-1246
*/

package com.ycs.tenjin.bridge.selenium.asv1.generic;


import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.UnreachableBrowserException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ycs.tenjin.TenjinConfiguration;
import com.ycs.tenjin.TenjinConfigurationException;
import com.ycs.tenjin.bridge.constants.BrowserType;
import com.ycs.tenjin.bridge.exceptions.DriverException;
import com.ycs.tenjin.util.Utilities;

public class Launcher {
	
	private static final Logger logger = LoggerFactory.getLogger(Launcher.class);
	
	public static WebDriver getDriver(String targetHostName, int targetPort, String browser) throws DriverException{
		
		/*****************************
		 * Fix for Defect#TEN-102
		 */
		if(!Utilities.trim(browser).equalsIgnoreCase(BrowserType.CHROME) && !Utilities.trim(browser).equalsIgnoreCase(BrowserType.FIREFOX) && !Utilities.trim(browser).equalsIgnoreCase(BrowserType.IE)&& !Utilities.trim(browser).equalsIgnoreCase(BrowserType.CE)){
			logger.error("Invalid Browser [{}]", browser);
			throw new DriverException("You have specified an invalid browser [" + browser + "]");
		}
		/*****************************
		 * Fix for Defect#TEN-102 ends
		 */
		
		if(Utilities.isLocalHost(targetHostName)){
			logger.info("Launching driver on Local Machine");
			return launchLocalDriver(browser);
		}else{
			logger.info("Launching driver on Remote Machine");
			return launchRemoteWebDriver(targetHostName, targetPort, browser);
		}
		
	}
	
	private static WebDriver launchRemoteWebDriver(String targetHostName, int targetPort, String browser) throws DriverException{
		URL url = null;
		
		DesiredCapabilities capabilities = null;
		WebDriver driver = null;
	
		try {
		
			url = new URL("http", targetHostName, targetPort, "/wd/hub");
			/***************************************************
			 * Change by Sriram to remove hardcoding target port (2-Dec-2014) ends
			 */
		

			
		if (browser.equalsIgnoreCase("Firefox")) {
			FirefoxOptions op =null;
			logger.debug("Setting capabilities for {}", browser);
			logger.info("Launching browser [{}]", browser);
			 op = new FirefoxOptions();
			op.setCapability("Marionette", true);
			op.setBinary(op.getBinary());
			//op.addArguments("--headless");
			op.addArguments("--no-sandbox");
			op.addArguments("--disable-dev-shm-usage");

			
				try{
					logger.info("Launching {} browser", browser);
					driver= new RemoteWebDriver(new URL(url.toString()), op);
					logger.info("Done");
					
					logger.info("Augmenting Driver");
					driver = new Augmenter().augment( driver );
					logger.info("Done");
					
					logger.info("Maximising");
					driver.manage().window().maximize();
					logger.info("Done");
					
				}catch(UnreachableBrowserException e){
					logger.error("UnReachableBrowserException caught",e);
					throw new DriverException("A problem occurred while launching " + browser + " browser on client " + targetHostName + ". Please contact Tenjin Support");
				}
			} 
			
			
		
		else if (browser.equalsIgnoreCase("Google Chrome")) {
			capabilities = DesiredCapabilities.chrome();
			ChromeOptions options = new ChromeOptions(); 
	        options.addArguments("--test-type");
	        options.addArguments("--allow-running-insecure-content");
	        /*capabilities.setCapability(ChromeOptions.CAPABILITY, options);*/

			
				try{
					logger.info("Launching {} browser", browser);
//					WebDriver driver = new RemoteWebDriver(new URL("http://www.example.com"), firefoxOptions);
//					driver = new RemoteWebDriver(url, capabilities);
					driver= new RemoteWebDriver(new URL(url.toString()), options);
					logger.info("Done");
					
					logger.info("Augmenting Driver");
					driver = new Augmenter().augment( driver );
					logger.info("Done");
					
					logger.info("Maximising");
					driver.manage().window().maximize();
					logger.info("Done");
				}catch(UnreachableBrowserException e){
					logger.error("UnReachableBrowserException caught",e);
					throw new DriverException("A problem occurred while launching " + browser + " browser on client " + targetHostName + ". Please contact Tenjin Support");
				}
			} 
		else if (browser.equalsIgnoreCase("Microsoft Internet Explorer")) {
			capabilities = DesiredCapabilities.internetExplorer();
			capabilities.setCapability(InternetExplorerDriver.IE_ENSURE_CLEAN_SESSION, false);
			capabilities.setCapability(InternetExplorerDriver.ENABLE_PERSISTENT_HOVERING, true);
			//capabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
		/*Modified by Ashiki for TENJINCG-1246 starts*/
		}else if ("Microsoft Edge".equalsIgnoreCase(browser)) {
			/*capabilities = DesiredCapabilities.internetExplorer();*/
			EdgeOptions option = new EdgeOptions();
			/*capabilities = DesiredCapabilities.edge();
			logger.info("Launching browser [{}]", browser);*/
			
				try{
					logger.info("Launching {} browser", browser);
					driver= new RemoteWebDriver(new URL(url.toString()), option);
					logger.info("Done");
					
					logger.info("Augmenting Driver");
					driver = new Augmenter().augment( driver );
					logger.info("Done");
					
					logger.info("Maximising");
					driver.manage().window().maximize();
					logger.info("Done");
					
				}catch(UnreachableBrowserException e){
					logger.error("UnReachableBrowserException caught",e);
					throw new DriverException("A problem occurred while launching " + browser + " browser on client " + targetHostName + ". Please contact Tenjin Support");
				}
			} 
		/*Modified by Ashiki for TENJINCG-1246 ends*/

		/*if (url != null) {
			try{
				logger.info("Launching {} browser", browser);
//				WebDriver driver = new RemoteWebDriver(new URL("http://www.example.com"), firefoxOptions);
//				driver = new RemoteWebDriver(url, capabilities);
				driver= new RemoteWebDriver(new URL(url.toString()), op);
				logger.info("Done");
				
				logger.info("Augmenting Driver");
				driver = new Augmenter().augment( driver );
				logger.info("Done");
				
				logger.info("Maximising");
				driver.manage().window().maximize();
				logger.info("Done");
			}catch(UnreachableBrowserException e){
				logger.error("UnReachableBrowserException caught",e);
				*//*******
				 * Changed message thrown to include details of browser type and target machine
				 *//*
				throw new DriverException("A problem occurred while launching " + browser + " browser on client " + targetHostName + ". Please contact Tenjin Support");
			}
		} else {
			*//*******
			 * Changed message thrown to include details of browser type and target machine
			 *//*
			throw new DriverException("A problem occurred while launching {} browser on client {}. Please contact Tenjin Support");
			throw new DriverException("A problem occurred while launching " + browser + " browser on client " + targetHostName + ". Please contact Tenjin Support");
		}*/
		} 
		
		catch (MalformedURLException e) {
			
			throw new DriverException("Invalid Client Host name. Could not launch Driver. Please verify your client information");
		}
		
		return driver;
	}
	
	@SuppressWarnings("deprecation")
	private static WebDriver launchLocalDriver(String browser) throws DriverException{
		
		try {
			WebDriver driver = null;
			DesiredCapabilities capabilities = null;
			if("Google Chrome".equalsIgnoreCase(browser)){
				
				logger.debug("Loading driver executable for {}", browser);
				//Changed by Sriram to remove hard-coding of browser driver exe path for local launches
				/*String exePath = "D:\\Tenjin\\client\\selenium_2.53.1\\browser_driver\\chromedriver_2.23.exe";*/
				String exePath = TenjinConfiguration.getProperty("SEL_DRIVER_PATH_CHROME");
				//Changed by Sriram to remove hard-coding of browser driver exe path for local launches ends
				System.setProperty("webdriver.chrome.driver", exePath);
				
				logger.debug("Setting capabilities for {}", browser);
				capabilities = DesiredCapabilities.chrome();
				ChromeOptions options = new ChromeOptions(); 
			    options.addArguments("--test-type");
			    options.addArguments("--allow-running-insecure-content");
			    capabilities.setCapability(ChromeOptions.CAPABILITY, options);
			    
			    logger.info("Launching browser [{}]", browser);
				driver = new ChromeDriver(capabilities);
			}else if("Firefox".equalsIgnoreCase(browser)){
				logger.info("Launching browser [{}]", browser);
				String exePath = TenjinConfiguration.getProperty("SEL_DRIVER_PATH_FIREFOX");
				System.setProperty("webdriver.gecko.driver", exePath);
				capabilities = DesiredCapabilities.firefox();
				FirefoxOptions opt = new FirefoxOptions(capabilities);
				logger.info("Launching browser [{}]", browser);
				driver = new FirefoxDriver(opt);
			}else if("Microsoft Internet Explorer".equalsIgnoreCase(browser)){

				logger.debug("Loading driver executable for {}", browser);
				String exePath = TenjinConfiguration.getProperty("SEL_DRIVER_PATH_IE");
				System.setProperty("webdriver.ie.driver", exePath);
				
				logger.debug("Setting capabilities for {}", browser);
				capabilities = DesiredCapabilities.internetExplorer();
				capabilities.setCapability(InternetExplorerDriver.ENABLE_PERSISTENT_HOVERING, true);
				
				logger.info("Launching browser [{}]", browser);
				driver = new InternetExplorerDriver(capabilities);
				/*Modified by Ashiki for TENJINCG-1246 starts*/
			}else if ("Microsoft Edge".equalsIgnoreCase(browser)) {
				logger.debug("Loading driver executable for {}", browser);
				String exePath = TenjinConfiguration.getProperty("SEL_DRIVER_PATH_CE");
				System.setProperty("webdriver.edge.driver", exePath);
				logger.debug("Setting capabilities for {}", browser);
				capabilities = DesiredCapabilities.edge();
				logger.info("Launching browser [{}]", browser);
				driver = new EdgeDriver(capabilities);
				/*Modified by Ashiki for TENJINCG-1246 ends*/
			}
			
			logger.info("Maximising");
			driver.manage().window().maximize();
			
			return driver;
		} catch(TenjinConfigurationException e){
			logger.error(e.getMessage());
			throw new DriverException(e.getMessage());
		}catch (Exception e) {
			
			logger.error("ERROR launching [{}] on local machine", browser, e);
			throw new DriverException("Could not launch " + browser + " browser on local machine.");
		}
		
	}
	
	
	
}
